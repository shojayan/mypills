//
//  Functions.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import Foundation


func getTabNumber()->Int{
    if let num =  UserDefaults.standard.value(forKey: "tabNumber"){
        return num as! Int
    }else{
        setTabNumber(numer: 0)
        return 0
    }
}

func setTabNumber(numer:Int){
    UserDefaults.standard.set(numer, forKey: "tabNumber")
    UserDefaults.standard.synchronize()
}
