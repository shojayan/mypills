//
//  Extensions.swift
//  MyPills
//
//  Created by omid on 1/30/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    
    enum DownloadImageState:Error {
        case Started
        case Finished
        case error
    }
    func downloadImage(from url: URL,status:@escaping (Result<DownloadImageState,DownloadImageState>,UIImage?)->Void) {
        
        
        
        func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
                  URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
              }
        
        
        status(.success(.Started),nil)
           getData(from: url) { data, response, error in
               guard let data = data, error == nil else {
                status(.failure(.error),nil)
                return }
               print(response?.suggestedFilename ?? url.lastPathComponent)
            
               DispatchQueue.main.async() {
                status(.success(.Finished),UIImage(data: data))
               }
           }
       }
}
