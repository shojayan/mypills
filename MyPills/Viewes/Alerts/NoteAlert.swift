//
//  NoteAlert.swift
//  MyPills
//
//  Created by omid on 2/3/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit

class NoteAlert: UIView , UITextViewDelegate{

    
    @IBOutlet var noteText:UITextView!
    
    typealias Action = (UITextView?)->Void
    
    var submitAction:Action?
    var cancelAction:Action?
    
    
    
    func setSettings(){
        noteText.delegate = self
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closKeyboard)))
    }
    
    @objc func closKeyboard(){
        endEditing(true)
    }
    @IBAction func submitAction(_ sender:Any){
        submitAction?(noteText)
        
    }
    
    @IBAction func cancelAction(_ sender:Any){
        cancelAction?(nil)
    }
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 0 {
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count < 1 {
            textView.text = "..." +  "Write note here" 
            textView.textColor = .gray
        }
    }
   
}
