//
//  FlowLayoutMaker.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import Foundation
import UIKit


class FlowLayoutMaker{
    
    fileprivate var collectionView:UICollectionView!
    fileprivate var minWidth:CGFloat!
    fileprivate var height:CGFloat!
    
    
    init(collectionView:UICollectionView, minWidth:CGFloat, height:CGFloat) {
        self.collectionView = collectionView
        self.minWidth = minWidth
        self.height = height
    }
    func getFlowLayout() -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        
        
        let availbleWidth = collectionView.bounds.inset(by: collectionView.layoutMargins).width
        let maxNum = Int(availbleWidth / minWidth)
        let width = (availbleWidth / CGFloat(maxNum)).rounded(.down)
        
        layout.itemSize = CGSize(width: width, height: height)
        
        // shoud be equal 0
        layout.minimumInteritemSpacing = CGFloat(0.0)
        layout.minimumLineSpacing = CGFloat(5.0)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        
        return layout
    }
}
