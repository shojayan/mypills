//
//  UIBlocker.swift
//  MyPills
//
//  Created by omid on 1/30/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit
import Lottie



class UIBlocker: UIView {

    
    @IBOutlet var lottiContainer:UIView!
    
    fileprivate let animation:AnimationView = {
        //19249-loading-circle-animation
        //19709-loading-double-dots
        //lf30_editor_h43gob
       return  AnimationView(name: "lf30_editor_h43gob")
    }()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    func initUIBlocker(){
        animation.frame = lottiContainer.bounds
        lottiContainer.addSubview(animation)
        animation.loopMode = .loop
    }
    
    func play(){
        animation.play()
    }
    
    func stop(){
        animation.stop()
    }
}
