//
//  CollectionViewCell.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userAddress: UILabel!
    
    @IBOutlet weak var orderStatus: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var indexPath:IndexPath = IndexPath(row: 0, section: 0 ) {
        didSet{
           
        }
    }
    
    var userOrder:UserOrder.UserOrderModel_data = UserOrder.UserOrderModel_data() {
        didSet{
            if self.userOrder.client_user != nil  {
                if self.userOrder.status != nil {
                    switch self.userOrder.status! {
                    case "1":
                        self.orderStatus.text = "Delivered"
                        self.orderStatus.textColor = #colorLiteral(red: 0.2196078431, green: 0.5568627451, blue: 0.2352941176, alpha: 1)
                        break
                    case "0":
                        self.orderStatus.text = "Pendin"
                        self.orderStatus.textColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
                    case "2":
                        self.orderStatus.text = "PickUp"
                        self.orderStatus.textColor = #colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 1)
                        break
                    case "-1":
                        self.orderStatus.text = "Failed"
                        self.orderStatus.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                        break
                    default:
                        break
                    }
                }
                self.userName.text = (self.userOrder.client_user?.full_name  ?? "")
                self.userAddress.text = self.userOrder.client_user?.address ?? ""
            }else{
               self.userName.text = ""
                self.userAddress.text = ""
            }
        }
    }
    

}
