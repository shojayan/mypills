//
//  Pen.swift
//  MyPills
//
//  Created by omid on 1/31/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import Foundation
import UIKit

class Pen{
    private var color:UIColor
    private var thickness:CGFloat
    
    init(penColor: UIColor, penThickness:CGFloat) {
        color = penColor
        thickness = penThickness
    }
    
    func changeColor(penColor: UIColor){
        color = penColor
    }
    
    func adjustThickness(penThickness: CGFloat){
        thickness = penThickness
    }
    
    func getColor() -> UIColor {
        return color
    }
    
    func getThickness() -> CGFloat {
        return thickness
    }
}
