//
//  MyPaintApp.swift
//  MyPills
//
//  Created by omid on 1/31/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import Foundation

import UIKit


struct Line {
    let color:UIColor
    let strockWidth:Float
    var points:[CGPoint]
}



class UIPaintView: UIView {

    fileprivate var lines = [Line]()
    
    
    fileprivate var strockColor:UIColor = .black
    fileprivate var strockWidth:Float = 5.0
  
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        guard let context = UIGraphicsGetCurrentContext() else {return}
        context.setLineCap(.round)
        
        
        lines.forEach { (line) in
            context.setStrokeColor(line.color.cgColor)
            context.setLineWidth(CGFloat(line.strockWidth))
            for (i,p) in line.points.enumerated() {
                if i == 0 {
                    context.move(to: p)
                    context.addLine(to: p)
                }else{
                    //context.addLine(to: p)
                    let previusePoint = line.points[i - 1]
                    let midPoint = midpoint(first: p , second: previusePoint)
                    context.addQuadCurve(to: p , control: midPoint)
                }
            }
            context.strokePath()
        }
    }
    
    
    func midpoint(first: CGPoint, second: CGPoint) -> CGPoint {
        let x = (first.x + second.x)/2
        let y = (first.y + second.y)/2
        return CGPoint(x: x, y: y)
    }
  
    func undoAction() {
        _ = lines.popLast()
        setNeedsDisplay()
    }
    func clearACtion(){
        lines.removeAll()
        setNeedsDisplay()
    }
    func setStrockColor(color:UIColor) {
        self.strockColor = color
    }
    func setStrockWdith(width:Float){
        self.strockWidth = width
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var line = Line(color: strockColor, strockWidth: strockWidth, points: [])
        guard let point = touches.first?.location(in: nil) else {return}
        line.points.append(point)
        lines.append(line)
        setNeedsDisplay()
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let point = touches.first?.location(in: nil) else {return}
        print(point)
        guard var lastLine = lines.popLast() else {return}
        lastLine.points.append(point)
        lines.append(lastLine)
        setNeedsDisplay()
    }
    

}


extension UIView {

    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
//    func asImage() -> UIImage {
//        if #available(iOS 10.0, *) {
//            let renderer = UIGraphicsImageRenderer(bounds: bounds)
//            return renderer.image { rendererContext in
//                layer.render(in: rendererContext.cgContext)
//            }
//        } else {
//            UIGraphicsBeginImageContext(self.frame.size)
//            self.layer.render(in:UIGraphicsGetCurrentContext()!)
//            let image = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//            return UIImage(cgImage: image!.cgImage!)
//        }
//    }
}

extension UIImage {
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        var newImage: UIImage?
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext(), let cgImage = self.cgImage {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(cgImage, in: newRect)
            if let img = context.makeImage() {
                newImage = UIImage(cgImage: img)
            }
            UIGraphicsEndImageContext()
        }
        return newImage
    }
    
    func scaled(to maxSize: CGFloat) -> UIImage? {
         let aspectRatio: CGFloat = min(maxSize / size.width, maxSize / size.height)
         let newSize = CGSize(width: size.width * aspectRatio, height: size.height * aspectRatio)
         let renderer = UIGraphicsImageRenderer(size: newSize)
         return renderer.image { context in
             draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
         }
     }
    
    func resize(targetSize: CGSize) -> UIImage {
    return UIGraphicsImageRenderer(size:targetSize).image { _ in
        self.draw(in: CGRect(origin: .zero, size: targetSize))
    }
    }
}



extension UIImage{
   func _resizeImage(targetSize: CGSize)-> UIImage?{
       let size = self.size
       
       let widthRatio = targetSize.width / size.width
       let heightRatio = targetSize.height / size.height
       
       var newSize: CGSize
       if widthRatio > heightRatio {
           newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
       } else {
           newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
       }
       let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
       
       UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
       self.draw(in: rect)
       let newImage = UIGraphicsGetImageFromCurrentImageContext()
       UIGraphicsEndImageContext()
       
       return newImage
   }
}
