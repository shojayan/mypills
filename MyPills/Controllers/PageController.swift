//PageViewcontroller
//  PageViewcontrollerViewController.swift
//  MypilsUIS
//
//  Created by omid on 1/28/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit

class PageViewcontroller: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {


    weak var pageViewDelegate: PageViewcontrollerDelegate?
    var index = 0
    private(set) lazy var viewControllers1: [UIViewController] = {
        // The view controllers will be shown in this order
        return [self.newColoredViewController("ToDoViewController"),
            self.newColoredViewController("HistoryviewController")
        ]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(changePageController), name: NSNotification.Name("changePageTo"), object: nil)
        
        
        self.dataSource = self
        self.delegate = self
        
        //self.setViewControllers([viewControllers1[0]], direction: .forward, animated: true, completion: nil )
        
        if getTabNumber() < viewControllers1.count{
            scrollToViewController(viewController: viewControllers1[getTabNumber()])
        }

        pageViewDelegate?.CustomageViewController(tutorialPageViewController: self, didUpdatePageCount: viewControllers1.count)
    }
    
    
    
    @objc func changePageController(_ sender:Notification){
        let key = sender.userInfo?["item"] as! Int
        scrollToViewController(index: key)
    }
    /**
       Scrolls to the next view controller.
       */
      func scrollToNextViewController() {
          if let visibleViewController = viewControllers?.first,
              let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
                scrollToViewController(viewController: nextViewController)
          }
      }
      
      /**
       Scrolls to the view controller at the given index. Automatically calculates
       the direction.
       
       - parameter newIndex: the new index to scroll to
       */
      func scrollToViewController(index newIndex: Int) {
          if let firstViewController = viewControllers?.first,
              let currentIndex = viewControllers1.firstIndex(of: firstViewController) {
              let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
                  let nextViewController = viewControllers1[newIndex]
                  scrollToViewController(viewController: nextViewController, direction: direction)
          }
      }
    func newColoredViewController(_ id: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: id)
    }
   
    /**
        Scrolls to the given 'viewController' page.
        
        - parameter viewController: the view controller to show.
        */
       private func scrollToViewController(viewController: UIViewController,
                                           direction: UIPageViewController.NavigationDirection = .forward) {
           setViewControllers([viewController],
               direction: direction,
               animated: true,
               completion: { (finished) -> Void in
                   // Setting the view controller programmatically does not fire
                   // any delegate methods, so we have to manually notify the
                   // 'tutorialDelegate' of the new index.
                DispatchQueue.main.async {
                    self.notifyTutorialDelegateOfNewIndex()
                }
           })
       }
    /**
     Notifies '_tutorialDelegate' that the current page index was updated.
     */
    private func notifyTutorialDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = viewControllers1.firstIndex(of: firstViewController) {
                pageViewDelegate?.CustomPageViewController(tutorialPageViewController: self, didUpdatePageIndex: index)
            NotificationCenter.default.post(name: NSNotification.Name("changeSegmentTo"), object: nil, userInfo: ["item":index])
            setTabNumber(numer:index)
        }
    }
   func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
    let index = viewControllers1.firstIndex(of: viewController)!
    if (index - 1 ) < 0   {
        //return viewControllers1[viewControllers1.count - 1]
        return nil
    }
    return viewControllers1[index - 1 ]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
         
        let index = viewControllers1.firstIndex(of: viewController)!
        
        if (index + 1 ) == viewControllers1.count   {
           // return viewControllers1[0]
            return nil
        }
        return viewControllers1[index + 1 ]
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
         notifyTutorialDelegateOfNewIndex()
    }
}

protocol PageViewcontrollerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter count: the total number of pages.
     */
    func CustomageViewController(tutorialPageViewController: PageViewcontroller,
        didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter tutorialPageViewController: the TutorialPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func CustomPageViewController(tutorialPageViewController: PageViewcontroller,
        didUpdatePageIndex index: Int)
    
}


