//
//  ViewController2.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit

class ToDoViewController: UICollectionViewController {

    
    let api_userOrder:UserOrder = UserOrder()
    var api_ordersData:UserOrder.UserOrderModel?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        let layout = FlowLayoutMaker(collectionView: collectionView, minWidth: 300.0, height: 97.5)
        collectionView.collectionViewLayout = layout.getFlowLayout()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
         // notification fire when refresh button from MAinCOntroller pressed and refresh collection view
        NotificationCenter.default.addObserver(self, selector: #selector(refreshList), name: NSNotification.Name("refreshTab0"), object: nil)
        retriveData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        api_ordersData = nil
        collectionView.reloadData()
    }
    
    @objc func refreshList(){
        retriveData()
    }
    func retriveData(){
        guard Reachability.isConnectedToNetwork() else {
            return
        }
        DispatchQueue.main.async {
             MainController.shared.mainviewController?.blockUI(isBlock: true)
        }
              //blockUI(isBlock: true, frame: view.frame)
        api_userOrder.getUserOrder(token:getUserData()!.access_token!, status:"0") { res in
                
                   switch res {
                   case .success(let val):
                    DispatchQueue.main.async {
                        self.api_ordersData = val
                        MainController.shared.mainviewController?.blockUI(isBlock: false)
                        self.collectionView.reloadData()
                    }
                       break
                   case .failure(let val):
                    DispatchQueue.main.async {
                        MainController.shared.mainviewController?.blockUI(isBlock: false)
                    }
                       switch val {
                       case .bodyGeneratorFailed:
                           print("bodyGeneratorFailed")
                           break
                       case .jsonParserError:
                           print("jsonParserError")
                           break
                       case .linkError:
                           print("linkError")
                           break
                       case .networkError:
                           print("networkError")
                           break
                       case .otherResponseCode:
                           print("otherResponseCode")
                           break
                       case .requiredValues:
                           print("requiredValues")
                           break
                       case .responseIsNill:
                           print("responseIsNill")
                           break
                       case .unSuccess:
                        print("unSuccess")
                        break
                       }
                   }
                   

               }
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.api_ordersData != nil, self.api_ordersData!.data != nil else {return 0 }
        
        return self.api_ordersData!.data!.count
    }
    
    
   override  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        
    cell.indexPath = indexPath
    
    cell.indexPath = indexPath
    cell.backgroundColor = .clear
    //cell.contentView.layer.cornerRadius = cell.contentView.bounds.height / 2.0
    cell.contentView.layer.masksToBounds = true
    cell.contentView.layer.borderWidth = 1
    cell.contentView.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    cell.contentView.layer.cornerRadius = 10
    
    cell.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    cell.layer.shadowOffset = CGSize(width: 0, height: 2)
    cell.layer.shadowRadius = 2.0
    cell.layer.shadowOpacity = 0.5
    cell.layer.masksToBounds = false
    cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
    cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    cell.layer.cornerRadius = 10
    cell.userOrder = self.api_ordersData!.data![indexPath.row]
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard api_ordersData != nil  else {
            return
        }
        
        MainController.shared.orderDetail = api_ordersData!.data![indexPath.row]
        MainController.shared.mainviewController?.performSegue(withIdentifier: "TodoDetail", sender: MainController.shared.mainviewController)
    }
}
