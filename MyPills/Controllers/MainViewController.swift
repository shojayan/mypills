//
//  ViewController.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    
    @IBOutlet var segmentController:UISegmentedControl!
    @IBOutlet var pagerContainer:UIView!
    
    let blockView = {
       return UINib(nibName: "UIBlocker", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIBlocker
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        MainController.shared.mainviewController = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeSegmentByNotiFyFromPageController), name: NSNotification.Name("changeSegmentTo"), object: nil)
    }
    

    @IBAction func refreshList(_ sender: Any) {
        
        if getTabNumber() == 0 {
            NotificationCenter.default.post(name: NSNotification.Name("refreshTab0"), object: nil)
        }else{
             NotificationCenter.default.post(name: NSNotification.Name("refreshTab1"), object: nil)
        }
    }
    
    @IBAction func segmentItemSelected(_ sender: UISegmentedControl) {
        notifyToChangePageControler(to: sender.selectedSegmentIndex)
    }
    
    @IBAction func logOUTAction(_ sender:Any){
       
        guard let _ = storyboard?.instantiateViewController(withIdentifier: "SplashViewController") else {
            return
        }
        
        
        
        let yesAction = UIAlertAction(title: "yes", style: .default) { alert  in
            DispatchQueue.main.async {
                self.present((self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController"))!, animated: true, completion: {
                       removeUserInformation()
                       })
            }
        }
        
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        
        createAlert(title: "Alert!", message: "Do you want to log-out your account ? ", actions: [yesAction, cancelAction])
        
       
        
    }
    
    func createAlert(title:String, message:String, actions:[UIAlertAction]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alert.addAction(action)
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    func blockUI(isBlock:Bool){
        blockView.initUIBlocker()
        blockView.frame = CGRect(x: 0, y: 0, width: self.pagerContainer.bounds.width, height: self.pagerContainer.bounds.height)
        
        blockView.needsUpdateConstraints()
        blockView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            if isBlock{
                self.pagerContainer.addSubview(blockView)
                self.blockView.play()
            }else{
                self.blockView.removeFromSuperview()
                self.blockView.stop()
            }
        
    }
    
    @objc func changeSegmentByNotiFyFromPageController(_ sender:Notification){
        let num = sender.userInfo?["item"] as! Int
        segmentController.selectedSegmentIndex = num 
    }
    
    func notifyToChangePageControler(to:Int){
        NotificationCenter.default.post(name: NSNotification.Name("changePageTo"), object: nil, userInfo: ["item":to])
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
