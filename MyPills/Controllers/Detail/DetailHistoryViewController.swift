//
//  ViewController3.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit
import Lottie

class DetailHistoryViewController: UIViewController, UICollectionViewDataSource{

     @IBOutlet var collectionView:UICollectionView!
     @IBOutlet var userSignContainer:UIImageView!
     @IBOutlet var loadingView:UIView!
     @IBOutlet var statusLable:UILabel!
    
    
    let indicator:AnimationView = {
       return AnimationView(name: "lf30_editor_h43gob")
    }()
    
     var titles_data:[[String]] = []
       
    
    
    
    
       override func viewDidLoad() {
        super.viewDidLoad()
        
        
      prepareViewController()
      showSignImage()
        
        
    }
    
    
    func loadingView(isShow:Bool){
        indicator.frame = loadingView.bounds
        indicator.loopMode = .loop
        loadingView.addSubview(indicator)
        
        if isShow{
            loadingView.isHidden = false
            indicator.play()
        }else{
            indicator.stop()
            loadingView.isHidden = true
        }
    }
    
    
    func showSignImage(){
        
        guard MainController.shared.orderDetail?.user_sign != nil  else {
            userSignContainer.image = #imageLiteral(resourceName: "noimage")
            return
        }
        
        
        
        loadingView(isShow: true)
        downloadImage(from: URL(string: mainImageURL + (MainController.shared.orderDetail?.user_sign)!)!) { res , img in
            switch res {
            case .success(let val):
                if val == .Started {
                    // nothing to do 
                }else if val == .Finished{
                    if img != nil {
                        DispatchQueue.main.async {
                            self.loadingView(isShow: false)
                            self.userSignContainer.image = img
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.loadingView(isShow: false)
                             self.userSignContainer.image = #imageLiteral(resourceName: "noimage")
                        }
                    }
                }else if val == .error {
                    DispatchQueue.main.async {
                        self.loadingView(isShow: false)
                         self.userSignContainer.image = #imageLiteral(resourceName: "noimage")
                    }
                }
                break
            case .failure(_):
                DispatchQueue.main.async {
                                       self.loadingView(isShow: false)
                                        self.userSignContainer.image = #imageLiteral(resourceName: "noimage")
                                   }
                break
            }
        }
    }
       
   
       func prepareViewController(){
           titles_data = [
               ["Client Name :",MainController.shared.orderDetail?.client_user?.full_name ?? ""],
               ["Client Number :",MainController.shared.orderDetail?.client_user?.phone_number ?? ""],
               ["Client Address :",MainController.shared.orderDetail?.client_user?.address ?? ""] ,
               ["Delivered by :",MainController.shared.orderDetail?.delivered_at ?? ""] ,
               ["Item count :",MainController.shared.orderDetail?.item_count ?? ""] ,
               ["Total Price :",MainController.shared.orderDetail?.total_price ?? ""],
               ["Payment Method :",MainController.shared.orderDetail?.payment_type ?? ""],
               ["Note :",MainController.shared.orderDetail?.note ?? ""]
           ]
           
           
           collectionView.register(UINib(nibName: "DetailCell", bundle: nil), forCellWithReuseIdentifier: "cell")
           let layout = UICollectionViewFlowLayout()
           layout.minimumLineSpacing = 2.0
           layout.minimumInteritemSpacing = 0.0
           
           layout.itemSize = CGSize(width: CGFloat(324) , height: CGFloat(75))
           if UIDevice.current.userInterfaceIdiom == .pad{
               layout.sectionInset = UIEdgeInsets(top: 50, left: 50, bottom: 0, right: 50)
           }
           collectionView.collectionViewLayout = layout
           collectionView.dataSource = self
        
        
        
        statusLable.layer.cornerRadius = 10
        statusLable.layer.masksToBounds = true
        if MainController.shared.orderDetail?.status! == "1" {
                   statusLable.text = "Delivered"
                   statusLable.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.5568627451, blue: 0.2352941176, alpha: 1)
               }else if MainController.shared.orderDetail?.status! == "0"{
                   statusLable.text = "Pendin"
                   statusLable.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
               }else if MainController.shared.orderDetail?.status! == "-1"{
                   statusLable.text = "Failed"
                   statusLable.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
               }
       }
       

       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return titles_data.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           
           
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DetailCell
           
           
           cell._title.text = titles_data[indexPath.row][0]
           cell.value.text = titles_data[indexPath.row][1]
           
           return cell
       }

}
