//
//  DetailTodoViewController.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit
import CoreLocation
import Lottie

class DetailTodoViewController: UIViewController, UICollectionViewDataSource, CLLocationManagerDelegate {

    
    @IBOutlet var collectionView:UICollectionView!
    @IBOutlet var drawPad:DrawingPad!
    @IBOutlet var loadingContainer:UIView!
    @IBOutlet var failedButton:UIButton!
    @IBOutlet var statusLable:UILabel!
    
    var pen:Pen!
    var titles_data:[[String]] = []
    
    
    let api_submitOrder:SetSuccessOrder = SetSuccessOrder()
    let api_submitNote:SetFailedOrder = SetFailedOrder()
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    var loadingView:AnimationView = {
       return AnimationView(name: "lf30_editor_h43gob")
    }()
    
    
    
    
    var alertNote:NoteAlert = {
        let alert =  UINib(nibName: "NoteAlert", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NoteAlert
        alert.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
        alert.noteText.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        alert.noteText.layer.borderWidth = 1
        alert.setSettings()
        alert.cancelAction = { _ in
            DispatchQueue.main.async {
                alert.noteText.text = "Write note here"
                alert.noteText.textColor = .gray
                alert.removeFromSuperview()
            }
        }
        return alert
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        collectionView.layer.cornerRadius = 10.0
        collectionView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        drawPad.layer.cornerRadius = 10.0
        drawPad.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        
        pen = Pen(penColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), penThickness: CGFloat(4.0))
        drawPad.aPen = pen
        prepareViewController()
        

        locManager.delegate = self
        locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
        //locManager.requestLocation()
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(sendNote), name: NSNotification.Name("sendNote"), object: nil)
        
        failedButton.layer.borderWidth = 1
        failedButton.layer.borderColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
    }
    
    
    @IBAction func clearAction(_ sender:Any){
//        drawPad.clearACtion()
//        drawPad.setNeedsDisplay()
        
      drawPad.clear()
    }
    
    
    
    func prepareViewController(){
        titles_data = [
            ["Client Name :",MainController.shared.orderDetail?.client_user?.full_name ?? ""],
            ["Client Number :",MainController.shared.orderDetail?.client_user?.phone_number ?? ""],
            ["Client Address :",MainController.shared.orderDetail?.client_user?.address ?? ""] ,
            ["Delivered by :",MainController.shared.orderDetail?.delivered_at ?? ""] ,
            ["Item count :",MainController.shared.orderDetail?.item_count ?? ""] ,
            ["Total Price :",MainController.shared.orderDetail?.total_price ?? ""],
            ["Payment Method :",MainController.shared.orderDetail?.payment_type ?? ""],
            ["Note :",MainController.shared.orderDetail?.note ?? ""]
        ]
        
        
        collectionView.register(UINib(nibName: "DetailCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 0.0
        
        layout.itemSize = CGSize(width: CGFloat(324) , height: CGFloat(75))
        if UIDevice.current.userInterfaceIdiom == .pad{
            layout.sectionInset = UIEdgeInsets(top: 50, left: 50, bottom: 0, right: 50)
        }
        collectionView.collectionViewLayout = layout
        collectionView.dataSource = self
        
        statusLable.layer.cornerRadius = 10
        statusLable.layer.masksToBounds = true
        if MainController.shared.orderDetail?.status! == "1" {
            statusLable.text = "Delivered"
            statusLable.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.5568627451, blue: 0.2352941176, alpha: 1)
        }else if MainController.shared.orderDetail?.status! == "0"{
            statusLable.text = "Pendin"
            statusLable.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
        }else if MainController.shared.orderDetail?.status! == "-1"{
            statusLable.text = "Failed"
            statusLable.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }
        
    }
    
    @IBAction func submitAction(_ sender: Any) {
        guard let _ = currentLocation else {
            createAlert(title: "Alert!", message: "Wait for getting your Location to submit ... ", actions: [UIAlertAction(title: "OK", style: .cancel, handler: nil)])
            return
        }
        noteAlert(iShow: true )
        alertNote.submitAction = { textView in
            DispatchQueue.main.async {
                self.alertNote.removeFromSuperview()
                NotificationCenter.default.post(name: NSNotification.Name("sendNote"), object: nil, userInfo: ["note":textView!.text ?? "","type":"submit"])
            }
        }
    }
    
    
    @IBAction func NoteAction(_ sender: Any) {
        noteAlert(iShow: true )
        alertNote.submitAction = { textView in
            DispatchQueue.main.async {
                self.alertNote.removeFromSuperview()
                NotificationCenter.default.post(name: NSNotification.Name("sendNote"), object: nil, userInfo: ["note":textView!.text ?? "","type":"note"])
            }
            
        }
    }
    
    
    
    
    func submitOrder(note:String){
        self.blockerUI(isSHow: true)
        api_submitOrder.setSuccessOrder(order_id: "\(MainController.shared.orderDetail?.id ?? 0 )",
               lat: "\((currentLocation!.coordinate.latitude))",
               lng: "\((currentLocation!.coordinate.longitude))",
               note: note,//MainController.shared.orderDetail?.note ?? "nil",
               user_signFile: drawPad.asImage(),
               token:getUserData()!.access_token!) { res in
                
                
                switch res {
                case .success(_):
                    DispatchQueue.main.async {
                        self.blockerUI(isSHow: false)
                        MainController.shared.mainviewController?.navigationController?.popViewController(animated: true)
                    }
                    break
                case .failure(let val):
                    DispatchQueue.main.async {
                    self.blockerUI(isSHow: false)
                    self.createAlert(title: "Error", message: "Faild to send, Pleas try again.", actions: [UIAlertAction(title: "OK", style: .cancel, handler: nil)])
                    }
                    switch val {
                    case .bodyGeneratorFailed:
                        print("bodyGeneratorFailed")
                        break
                    case .jsonParserError:
                        print("jsonParserError")
                        break
                    case .linkError:
                        print("linkError")
                        break
                    case .networkError:
                        print("networkError")
                        break
                    case .otherResponseCode:
                        print("otherResponseCode")
                        break
                    case .requiredValues:
                        print("requiredValues")
                        break
                    case .responseIsNill:
                        print("responseIsNill")
                        break
                    case .unSuccess:
                        print("unSuccess")
                        break
                    }
                    break
                }
                
        }
    }
    func faileOrder(note:String){
        guard let _ = MainController.shared.orderDetail?.id else {
            createAlert(title: "Alert!", message: "order id not valid, try again later. ", actions: [UIAlertAction(title: "OK", style: .cancel, handler: nil)])
            return
        }
        blockerUI(isSHow: true)
        api_submitNote.setFailedOrder(token: getUserData()!.access_token!,
                                      order_id: "\((MainController.shared.orderDetail?.id)!)", note: note) { res in
                                        switch res {
                                        case .success(_ ):
                                            DispatchQueue.main.async {
                                                self.blockerUI(isSHow: false)
                                                MainController.shared.mainviewController?.navigationController?.popViewController(animated: true)
                                            }
                                            break
                                        case .failure(_):
                                            DispatchQueue.main.async {
                                                self.blockerUI(isSHow: false)
                                                self.createAlert(title: "Alert!", message: "failed to send note, check your internet connection, then try again", actions: [UIAlertAction(title: "OK", style: .cancel, handler: nil)])
                                            }
                                            break
                                        }
        }
    }
    
    
    
    @objc func sendNote(_ sender:Notification){
        
        let note = sender.userInfo?["note"] as! String
        let type = sender.userInfo?["type"] as! String
        
        if type == "submit" {
            submitOrder(note: note)
        }else if type == "note" {
            faileOrder(note: note)
        }
        
    }
    func noteAlert(iShow:Bool){
        alertNote.frame = view.bounds
        if iShow{
            view.addSubview(alertNote)
        }else{
            alertNote.removeFromSuperview()
        }
    }
    func createAlert(title:String, message:String, actions:[UIAlertAction]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alert.addAction(action)
        }
        
        present(alert, animated: true, completion: nil)
    }
    func blockerUI(isSHow:Bool){
        loadingView.frame = loadingContainer.bounds
        loadingView.loopMode = .loop
        loadingContainer.addSubview(loadingView)
        
        if isSHow{
            loadingContainer.isHidden = false
            loadingView.play()
            view.setNeedsLayout()
        }else{
            loadingView.stop()
            loadingContainer.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles_data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DetailCell
        
        
        cell._title.text = titles_data[indexPath.row][0]
        cell.value.text = titles_data[indexPath.row][1]
        
        return cell
    }

    
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation

        /* you can use these values*/
        currentLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
}
