//
//  SplashViewController.swift
//  MyPills
//
//  Created by omid on 2/1/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit
import Lottie


class SplashViewController: UIViewController {

    
    @IBOutlet var animationView:UIView!
    @IBOutlet var versionLbl:UILabel!
    
    
    let animationV:AnimationView = {
       return   AnimationView(name: "16475-line-art-of-city-landscape-and-landmark")
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animationView.addSubview(animationV)
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"]!
        let _ = Bundle.main.infoDictionary!["CFBundleVersion"]!
        versionLbl.text =  (version as? String) != nil ? "Version : " +  (version as? String)! : "" 
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animationV.frame = animationView.bounds
        animationV.play { _ in
            DispatchQueue.main.async {
                self.routTo()
            }
        }
    }
    
    
    func routTo(){
        if getUserData() != nil {
            performSegue(withIdentifier: "toMainController", sender: nil)
        }else {
            performSegue(withIdentifier: "toLogIn", sender: nil)
        }
    }
    
}
