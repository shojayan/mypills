//
//  SendValidationNumer.swift
//  MyPills
//
//  Created by omid on 2/1/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit

class SendValidationNumerView: UIView {

    @IBOutlet var codeTxt:UITextField!
    @IBOutlet var counterDigit:UILabel!
    @IBOutlet var counterDigitSecond:UILabel!
    @IBOutlet weak var sendAgainBtn: UIButton!
    @IBOutlet weak var errorLable: UILabel!
    
    
    typealias ActionType = ()->Void
    var sendBtnAction:ActionType?
    var exitFromLogIn:ActionType?
    
    
    var timer:Timer?
    var isSendCodeState:Bool?
    
    
    let api_sendVerifyCode:SetKey = {
       return SetKey()
    }()
    var phoneNumber:String!
    
    lazy var validateNumberLable:UILabel = {
           let lbl = UILabel()
           return lbl
       }()
    
    fileprivate let blockerView:UIBlocker = {
        let blocker = UINib(nibName: "UIBlocker", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIBlocker
        blocker.initUIBlocker()
        blocker.play()
        return blocker
    }()
    
    var killTimer:Bool = false
    
    @IBAction func sendButtonAction(_ sender:Any){
        uiBlocker(isShow: true)
        self.closeKeyBoard()
        api_sendVerifyCode.setkey(phoneNumber: phoneNumber,
                                  valueKey: codeTxt.text!) { res in
                                    switch res{
                                    case .success(let val):
                                        setUserInformation(data: val)
                                        DispatchQueue.main.async {
                                            self.uiBlocker(isShow: false)
                                            //self.sendBtnAction?()
                                            self.exitFromLogIn?()
                                            self.killTimer = true
                                        }
                                        break
                                    case .failure(let val):
                                        switch val {
                                        case .otherResponseCode:
                                            DispatchQueue.main.async {
                                                self.uiBlocker(isShow: false)
                                                self.errorLable.text = "Entred code is invalid"
                                            }
                                            break
                                        default:
                                            DispatchQueue.main.async {
                                                self.uiBlocker(isShow: false)
                                                self.errorLable.text = "Other Error acured"
                                            }
                                            break
                                        }
                                        break
                                    }
        }
    }

    @IBAction func sendAgainAction(_ sender: Any) {
        sendBtnAction?()
        sendAgainBtn.isHidden = true
    }
    
    
    
    @IBAction func closeAction(_ sender: Any) {
        self.sendBtnAction?()
        killTimer = true
    }
    
    
    func validateNumberLable(isSHow:Bool,number:String){
        validateNumberLable.frame = CGRect(origin: .zero, size: CGSize(width: bounds.width, height: 100))
        validateNumberLable.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        validateNumberLable.textColor = .black
        validateNumberLable.alpha = 0
        validateNumberLable.textAlignment = .center
        validateNumberLable.text = number
        if isSHow {
            addSubview(validateNumberLable)
            UIView.animate(withDuration: 0.5, delay: 2.0, options: [.allowUserInteraction], animations: {
                 self.validateNumberLable.alpha = 1
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.validateNumberLable.alpha = 0
            }){ _ in
                self.validateNumberLable.removeFromSuperview()
            }
        }
    }
    
    
    
    
    
    
    func setSettings(){
        codeTxt.addTarget(self, action: #selector(textCodeChanged), for: .editingDidBegin)
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeKeyBoard)))
    }
    
    @objc func closeKeyBoard(){
        endEditing(true)
    }
    @objc func textCodeChanged(_ sender:UITextField){
        errorLable.text = ""
    }
    func uiBlocker(isShow:Bool){
        blockerView.frame = frame
        blockerView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        if isShow{
            addSubview(blockerView)
        }else{
            blockerView.removeFromSuperview()
        }
    }
    
    @objc func timerObserver(_ sender:Notification){
        print("timer fired")
        //fireTimer(second: 30)
    }
    func fireTimer(second time:Int){
        var time = time
        self.counterDigit.text = "\(time)"
        self.counterDigit.isHidden = false
        self.counterDigitSecond.isHidden = false
        sendAgainBtn.isHidden = true
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            time -= 1
            print(time)
            self.counterDigit.text = "\(time)"
            if time == 0 {
                self.counterDigit.isHidden = true
                self.counterDigitSecond.isHidden = true
                self.sendAgainBtn.isHidden = false
                
                self.validateNumberLable(isSHow: false, number: "")
                timer.invalidate()
            }
            
            if self.killTimer {
                timer.invalidate()
            }
        }
    }
    
    
}

//
//struct SetKeyModel:Codable {
//     var token_type:String
//     var expires_in: Int
//     var access_token:String
//     var refresh_token:String
// }
func setUserInformation(data:SetKey.SetKeyModel){
    UserDefaults.standard.set(data.token_type, forKey: "userData_token_type")
    //UserDefaults.standard.set(data.expires_in, forKey: "userData_expires_in")
    UserDefaults.standard.set(data.access_token, forKey: "userData_access_token")
    UserDefaults.standard.set(data.refresh_token, forKey: "userData_refresh_token")
}
func removeUserInformation(){
    UserDefaults.standard.set(nil, forKey: "userData_token_type")
    //UserDefaults.standard.set(data.expires_in, forKey: "userData_expires_in")
    UserDefaults.standard.set(nil, forKey: "userData_access_token")
    UserDefaults.standard.set(nil, forKey: "userData_refresh_token")
}

func getUserData()->SetKey.SetKeyModel?{
    guard UserDefaults.standard.value(forKey: "userData_token_type") != nil else {
        return nil
    }
//    guard UserDefaults.standard.value(forKey: "userData_expires_in") != nil else {
//           return nil
//       }
    guard UserDefaults.standard.value(forKey: "userData_access_token") != nil else {
           return nil
       }
    guard UserDefaults.standard.value(forKey: "userData_refresh_token") != nil else {
           return nil
       }
    return SetKey.SetKeyModel(token_type: (UserDefaults.standard.value(forKey: "userData_token_type") as! String),
                              expires_in: nil,
                              access_token: (UserDefaults.standard.value(forKey: "userData_access_token") as! String),
                              refresh_token: (UserDefaults.standard.value(forKey: "userData_refresh_token") as! String))
}
