//
//  ViewController.swift
//  MyPills
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omid. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {

    
    @IBOutlet var phoneNumberText:UITextField!
    @IBOutlet var errorText:UILabel!
    
    var validateView:SendValidationNumerView = {
        let validateView = UINib(nibName: "SendValidationNumerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SendValidationNumerView
        validateView.setSettings()
        return validateView
    }()
    
   
    
    
    var stateOfView = true
    let api_setNumber:SetPhtoneNumber = {
       return SetPhtoneNumber()
    }()
    
    var validateNumber:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareValidationView()
        phoneNumberText.addTarget(self, action: #selector(textChanged), for: .editingDidBegin)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeKeyBoard)))
    }
    
    @IBAction func logInAction(_ sender: Any) {
        self.closeKeyBoard()
        if stateOfView{
            if phoneNumberText.text!.count > 10 {
                api_setNumber.setPhoneNumber(phoneNumber: phoneNumberText.text!) { res in
                    switch res {
                    case .success(let val ):
                        self.stateOfView = !self.stateOfView
                        DispatchQueue.main.async {
                            self.validateNumber = "\(val.data!.key!)"
                            self.verifyView(isShow: true)
                        }
                        print(val)
                        break
                    case .failure(_):
                        DispatchQueue.main.async {
                            self.errorText.text = "Error in sending number, Try again"
                        }
                        break
                    }
                }
            }else{
                errorText.text = "Please enter your number first! "
            }
        }
    }
    
    @objc func closeKeyBoard(){
        view.endEditing(true)
    }
    @objc func textChanged(_ sender:UITextField){
        errorText.text = ""
    }
    
    
    func prepareValidationView(){
       
        validateView.frame = view.bounds
        if UIDevice.current.userInterfaceIdiom == .phone {
            (validateView.viewWithTag(200) as! UILabel).font = UIFont.boldSystemFont(ofSize: CGFloat(40))
        }
        
        validateView.sendBtnAction = {
            if !self.stateOfView{
                self.stateOfView = !self.stateOfView
                self.verifyView(isShow: false)
            }
        }
        
        validateView.exitFromLogIn = {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func verifyView(isShow:Bool){
        if isShow{
            UIView.transition(from: view, to: validateView, duration: 1.0, options: [.transitionCurlDown]){ _ in
                self.validateView.killTimer = false
                self.validateView.phoneNumber = self.phoneNumberText.text!
                self.validateView.fireTimer(second: 30)
                self.validateView.validateNumberLable(isSHow: true, number: self.validateNumber)
            }
        }else{
            UIView.transition(from: validateView, to: view, duration: 1.0, options: [.transitionCurlUp]){ _ in
                self.validateView.validateNumberLable(isSHow: false, number: self.validateNumber)
            }
        }
    }
    
}

