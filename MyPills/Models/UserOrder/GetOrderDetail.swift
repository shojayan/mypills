//
//  GetOrderDetail.swift
//  API
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation



class GetOrderDetail {
    
    
    
    
    struct GetOrderDetailModel:Codable {
        var success:Bool?
        var message:String?
        var data:GetOrderDetailModel_data?
    }
    struct GetOrderDetailModel_data:Codable{
        var id:Int?
        var delivery_user_id:String?
        var client_user_id:String?
        var status:String?
        var item_count:String?
        var total_price:String?
        var lat:String?
        var lng:String?
        var user_sign:String?
        var note:String?
        var delivered_at:String?
        var payment_type:String?
        var created_at:String?
        var updated_at:String?
    }
    
    
    
    
    func getPrderDetail(order_id:String, token:String, completion:@escaping (Result<GetOrderDetailModel,APIError>)->Void){
        
        let semaphore = DispatchSemaphore (value: 0)

        // first check the url string is ok for makeing URL from String
        guard let link = "\(urlConstOrder)/orderDetails?order_id=\(order_id)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
            completion(.failure(.linkError))
            return
        }
        var request = URLRequest(url: URL(string: link)!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            semaphore.signal()
            completion(.failure(.networkError))
            return
          }
          do {
              let res = response as? HTTPURLResponse
            if res != nil {
                if res!.statusCode == 200 {
                    let jsonDecoder = JSONDecoder()
                    let model = try jsonDecoder.decode(GetOrderDetailModel.self, from: data)
                    if model.success! {
                        completion(.success(model))
                    }else{
                        completion(.failure(.unSuccess))
                    }
                }else{
                    completion(.failure(.otherResponseCode))
                }
            }else {
                completion(.failure(.responseIsNill))
            }
          } catch  {
            completion(.failure(.jsonParserError))
          }
          semaphore.signal()
        }
        task.resume()
        semaphore.wait()

    
    }
}
