//
//  SetFailedOrder.swift
//  API
//
//  Created by omid on 1/29/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation




class SetFailedOrder {
    
    
    
    struct SetFailedOrderModel:Codable {
        var success:Bool
        var message:String
        //var data:?
    }
    
    
    func setFailedOrder(token:String, order_id:String, note:String, completion: @escaping (Result<SetFailedOrderModel,APIError>)->Void){

        let semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "order_id",
            "value": order_id,
            "type": "text"
          ],
          [
            "key": "note",
            "value": note,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-675AFEWR-H2AT-4588-ADEF-769FA7C3F890"//"Boundary-\(UUID().uuidString)"

        var request = URLRequest(url: URL(string: "\(urlConstOrder)/setFailed")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = formBodyGenerator(parameters: parameters, boundary: boundary)

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            semaphore.signal()
            completion(.failure(.networkError))
            return
          }
            do{
                let res = response as? HTTPURLResponse
                if res != nil {
                    if res!.statusCode == 200 {
                        let jasonDecoder = JSONDecoder()
                        let model = try jasonDecoder.decode(SetFailedOrderModel.self, from: data)
                        if model.success {
                            completion(.success(model))
                        }else{
                            completion(.failure(.unSuccess))
                        }
                    }else{
                        completion(.failure(.otherResponseCode))
                    }
                }else{
                    completion(.failure(.responseIsNill))
                }
            }catch{
                completion(.failure(.jsonParserError))
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
}
