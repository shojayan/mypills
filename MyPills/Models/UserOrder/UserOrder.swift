//
//  UserOrder.swift
//  API
//
//  Created by omidshojaeian on 1/24/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation




class UserOrder {
    struct UserOrderModel:Codable {
        var success:Bool?
        var message:String?
        var data:[UserOrderModel_data]?
    }
   struct UserOrderModel_data:Codable {
            var id: Int?
            var status: String?
            var item_count: String?
            var total_price: String?
            var lat: String?
            var lng: String?
            var user_sign: String?
            var note: String?
            var delivered_at: String?
            var payment_type: String?
            var client_user:UserOrderModel_data_client_user?
    }
    struct UserOrderModel_data_client_user:Codable{
        var id:Int?
        var full_name:String?
        var phone_number:String?
        var address:String?
    }
    
    
    func getUserOrder(token:String, status:String? = nil, completion:@escaping (Result<UserOrderModel,APIError>)->Void ){

        //let semaphore = DispatchSemaphore (value: 0)

        var urlComponent = URLComponents(string: "\(urlConstOrder)")
        urlComponent?.queryItems = [URLQueryItem(name: "status", value: status)]
        var request = URLRequest(url: urlComponent!.url!,timeoutInterval: 10)
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            //semaphore.signal()
            completion(.failure(.networkError))
            return
          }
            do{
                let res = response as? HTTPURLResponse
                if res != nil {
                    if res!.statusCode == 200 {
                        let jsonDecoder = JSONDecoder()
                        let model = try jsonDecoder.decode(UserOrderModel.self, from: data)
                        if model.success! {
                            completion(.success(model))
                        }else{
                            completion(.failure(.unSuccess))
                        }
                    }else {
                        completion(.failure(.otherResponseCode))
                    }
                }else{
                    completion(.failure(.responseIsNill))
                }
            }catch{
                completion(.failure(.jsonParserError))
            }
          //semaphore.signal()
        }

        task.resume()
        //semaphore.wait()

    }
}
