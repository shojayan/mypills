//
//  SetSuccessOrder.swift
//  API
//
//  Created by omidshojaeian on 1/24/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation
import UIKit



class SetSuccessOrder {
    struct SetSuccessOrdeModel:Codable{
        var success:Bool
        var message:String
        //var data:?
    }
    
    
    func setSuccessOrder(order_id:String,
                         lat:String,
                         lng:String,
                         note:String,
                         user_signFile:UIImage,
                         token:String,
                         completion:@escaping (Result<SetSuccessOrdeModel,APIError>)->Void){
        
        

        guard order_id.count > 0 , lat.count > 0 , lng.count > 0 , note.count > 0  else {
            completion(.failure(.requiredValues))
            return
        }
        print("secssefull")
        let parameters:[String : String] = [
            "order_id": order_id,
            "lat": lat,
            "lng": lng,
            "note": note
            ]
        
        let multiPartAPI = Multi_part_Uploader()
        // http://127.0.0.1:5000/uploader
        // "\(urlConstOrder)/setSuccess"
        multiPartAPI.requestSingleFile(stringUrl: "\(urlConstOrder)/setSuccess"
        ,parameters: parameters
        ,fileKey: "user_sign"
        ,imageNames: ["sign.png"]
        ,token:token
        ,images: [user_signFile.jpegData(compressionQuality: 1.0)!]) { data, response, error in
            
            guard let data = data else {
                completion(.failure(.networkError))
                return
            }
            
            
            print(String(data: data, encoding: .utf8) ?? "data from setSeccessOrder API")
            do{
                let jasonDecoder = JSONDecoder()
                let res = response as? HTTPURLResponse
                if res != nil {
                    if res!.statusCode == 200 {
                        
                        let model = try jasonDecoder.decode(SetSuccessOrdeModel.self, from: data)
                        if model.success {
                            completion(.success(model))
                        }else{
                            completion(.failure(.unSuccess))
                        }
                        
                    }else{
                        print(res?.statusCode ?? "")
                        print(String(data: data, encoding: .utf8) ?? errorAPI() )
                        completion(.failure(.otherResponseCode))
                    }
                }else{
                    completion(.failure(.responseIsNill))
                }
            }catch{
                print((response as? HTTPURLResponse)?.statusCode ?? "")
                print(String(data: data, encoding: .utf8) ?? errorAPI() )
                completion(.failure(.jsonParserError))
            }
        }
        
    }
}
extension UIView {

    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}
