//
//  UtilityFunctions.swift
//  API
//
//  Created by omidshojaeian on 1/24/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation


let mainImageURL = "http://e-meds.ca/upload/user-sign/"
let urlConstUser = "http://e-meds.ca/api/v1/user"
let urlConstOrder = "http://e-meds.ca/api/v1/order"





func formBodyGenerator(parameters:[[String:Any]], boundary:String )->Data? {
    //let boundary = "Boundary-675AFEWR-H2AT-4588-ADEF-769FA7C3F890"//"Boundary-\(UUID().uuidString)"
    var body = ""
    //var error: Error? = nil
    for param in parameters {
      if param["disabled"] == nil {
        let paramName = param["key"]!
        body += "--\(boundary)\r\n"
        body += "Content-Disposition:form-data; name=\"\(paramName)\""
        let paramType = param["type"] as! String
        if paramType == "text" {
          let paramValue = param["value"] as! String
          body += "\r\n\r\n\(paramValue)\r\n"
        } else {
          let paramSrc = param["src"] as! String
          let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
            
            // check fileData not be nil 
            guard fileData != nil else {return nil }
            
          let fileContent = String(data: fileData!, encoding: .utf8)!
          body += "; filename=\"\(paramSrc)\"\r\n"
            + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
        }
      }
    }
    body += "--\(boundary)--\r\n";
    return body.data(using: .utf8)
}


struct errorAPI:Codable {
    var success:Bool?
    var message:String?
    var data:[String]?
}
func errorDecoder(data:Data)->errorAPI?{
    let jsonDecoder = JSONDecoder()
    do {
        return try jsonDecoder.decode(errorAPI.self, from: data)
    } catch  {
        return nil
    }
}

enum APIError:Error{
    case jsonParserError
    case networkError
    case responseIsNill
    case otherResponseCode
    case bodyGeneratorFailed
    case requiredValues
    case linkError
    case unSuccess
}
