//
//  GetUserInformation.swift
//  API
//
//  Created by omidshojaeian on 1/24/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation


class GetUserInformation {
    
    struct UserInfoModel:Codable {
        var success:Bool?
        var message:String?
        var data:UserInfoModel_data?
    }
    
    struct UserInfoModel_data:Codable  {
        var id:Int?
        var full_name:String?
        var phone_number:String?
    }
    
    
    func getUserInfo(token:String,completion:@escaping (Result<UserInfoModel,APIError>)->Void){

        let semaphore = DispatchSemaphore (value: 0)
        var request = URLRequest(url: URL(string: urlConstUser)!,timeoutInterval: 10)
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            semaphore.signal()
            completion(.failure(.networkError))
            return
          }
            do{
                let res = response as? HTTPURLResponse
                if res != nil {
                    if res!.statusCode == 200 {
                        let jsonDecoder = JSONDecoder()
                        let model = try jsonDecoder.decode(UserInfoModel.self, from: data)
                        if model.success! {
                            completion(.success(model))
                        }else{
                            completion(.failure(.unSuccess))
                        }
                    }else{
                        completion(.failure(.otherResponseCode))
                    }
                }else{
                    completion(.failure(.responseIsNill))
                }
            }catch{
                completion(.failure(.jsonParserError))
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
}
