//
//  setKey.swift
//  API
//
//  Created by omidshojaeian on 1/24/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation


class SetKey {
    struct SetKeyModel:Codable {
        var token_type:String?
        var expires_in: Int?
        var access_token:String?
        var refresh_token:String?
    }
    
    func setkey(phoneNumber:String, valueKey:String, completion:@escaping (Result<SetKeyModel, APIError>)->Void){

        let semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "phone_number",
            "value": phoneNumber,
            "type": "text"
          ],
          [
            "key": "key",
            "value": valueKey,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-675AFEWR-H2AT-4588-ADEF-769FA7C3F890"//"Boundary-\(UUID().uuidString)"
        let postData = formBodyGenerator(parameters: parameters, boundary: boundary)

        var request = URLRequest(url: URL(string: "\(urlConstUser)/checkKey")!,timeoutInterval: 10)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            semaphore.signal()
            completion(.failure(.networkError))
            return
          }
            do {
                let res = response as? HTTPURLResponse
                if res != nil {
                    if res!.statusCode == 200 {
                        let jsonDecoder = JSONDecoder()
                        completion(.success(try jsonDecoder.decode(SetKeyModel.self, from: data)))
                    }else{
                        completion(.failure(.otherResponseCode))
                    }
                }else{
                    completion(.failure(.responseIsNill))
                }
            }catch{
                completion(.failure(.jsonParserError))
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()

    }
    
}
