//
//  User_APIes.swift
//  API
//
//  Created by omidshojaeian on 1/24/1399 AP.
//  Copyright © 1399 AP omidshojaeian. All rights reserved.
//

import Foundation




class SetPhtoneNumber {
    struct setPhoneNumberModel:Codable {
        
            var success: Bool?
            var message: String?
            var data:setPhoneNumberModel_data?
    }
    struct setPhoneNumberModel_data:Codable{
            var key: Int?
    }
    func setPhoneNumber(phoneNumber:String,completion: @escaping (Result<setPhoneNumberModel,APIError>)->Void){
        let semaphore = DispatchSemaphore (value: 0)
        let parameters = [
          [
            "key": "phone_number",
            "value": phoneNumber,
            "type": "text"
          ]] as [[String : Any]]
        
        // make a random boundary string like this format
        let boundary = "Boundary-675AFEWR-H2AT-4588-ADEF-769FA7C3F890"//"Boundary-\(UUID().uuidString)"
        let postData = formBodyGenerator(parameters: parameters, boundary: boundary)
        //
        var request = URLRequest(url: URL(string: "\(urlConstUser)/setPhoneNumber")!,timeoutInterval: 10)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            semaphore.signal()
            completion(.failure(.networkError))
            return
          }
          //print(String(data: data, encoding: .utf8)!)
            do {
                let res = response as? HTTPURLResponse
                if res != nil {
                    if res!.statusCode == 200 {
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(setPhoneNumberModel.self, from: data)
                        if model.success! {
                            completion(.success(model))
                        }else{
                            completion(.failure(.unSuccess))
                        }
                    }else{
                        completion(.failure(.otherResponseCode))
                    }
                    
                }else {
                    completion(.failure(.responseIsNill) )
                }
            }catch{
                completion(.failure(.jsonParserError))
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
    
}
